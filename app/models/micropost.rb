class Micropost < ActiveRecord::Base
  belongs_to :user
  validates :context, length: { maximum: 140 } # Context should instead be content, modify schema to change that and run bundle exec rake db:migrate to fix it
end
